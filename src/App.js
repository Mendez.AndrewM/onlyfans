import React, { useState, useEffect } from 'react';
// import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import LoadingScreen from './pages/LoadingScreen';
import Landing from './pages/Landing';
import './assets/styles/main.scss'


function App() {

  // const [userKey, setUserKey] = useState('')
  const [theme, setTheme] = useState('light');
  const [page, setPage] = useState('');
  const dark = theme === 'dark'

  useEffect(() => {
    setTimeout(() => {
      setPage('yass');
    }, 3000)
  }, []);

  return (
    <div className="App">
      {
        !page && (
          <LoadingScreen theme={theme} setTheme={setTheme}>
            <p className="ComingSoon"> Coming Soon - Under Construction</p>
          </LoadingScreen>
        )
      }
      {page && <Landing />}
    </div>
  //   <Router>
  //   <div>
  //     <Nav />
  //     <Switch>
  //       <Route exact path={["/", "/books"]}>
  //         <Books />
  //       </Route>
  //       <Route exact path="/books/:id">
  //         <Detail />
  //       </Route>
  //       <Route>
  //         <NoMatch />
  //       </Route>
  //     </Switch>
  //   </div>
  // </Router>
  );
}




export default App;